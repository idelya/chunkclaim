package net.idelya.chunk_claim;

import net.idelya.chunk_claim.database.Database;
import net.idelya.chunk_claim.database.exception.DatabaseException;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;

public class ChunkClaimPlugin extends JavaPlugin {
    private final Database database;

    public ChunkClaimPlugin() {
        this.database = new Database(this.getLogger());
    }

    @Override
    public void onEnable() {
        // Write the default configuration if it's not exist in plugin folder
        this.saveDefaultConfig();

        this.connectDatabase();
    }

    @Override
    public void onDisable() {
        this.database.disconnect();
    }

    /**
     * Connect to the database with parameters in configuration.
     */
    private void connectDatabase() {
        this.getLogger().info("Trying to connect to the database.");

        try {
            // Get the database configuration
            ConfigurationSection mysqlConfig = this.getConfig().getConfigurationSection("mysql");

            if (mysqlConfig == null) {
                // Create default configuration
                mysqlConfig = this.getConfig().createSection("mysql");

                mysqlConfig.set("host", "localhost");
                mysqlConfig.set("port", 3306);
                mysqlConfig.set("database", "database");
                mysqlConfig.set("username", "user");
                mysqlConfig.set("password", "P@ssw0rd");

                this.saveConfig();
            }

            String host = mysqlConfig.getString("host");
            int port = mysqlConfig.getInt("port", 0);
            String database = mysqlConfig.getString("database");
            String username = mysqlConfig.getString("username");
            String password = mysqlConfig.getString("password");

            if (host == null) {
                throw new DatabaseException("The database host cannot be empty in configuration.");
            }
            if (port == 0) {
                throw new DatabaseException("The database port cannot be empty in configuration.");
            }
            if (database == null) {
                throw new DatabaseException("The database name cannot be empty in configuration.");
            }
            if (username == null) {
                throw new DatabaseException("The database username cannot be empty in configuration.");
            }
            if (password == null) {
                throw new DatabaseException("The database password cannot be empty in configuration.");
            }

            // Connect to database with credentials in configuration
            boolean isConnected = this.database.connect(host, port, database, username, password);

            if (!isConnected) {
                throw new DatabaseException("Can't connect to database, check the configuration.");
            }

            // Make the database schema up-to-date
            this.database.applyMigrations();

        } catch (DatabaseException e) {
            this.getLogger().severe(e.getMessage());
        }
    }
}
