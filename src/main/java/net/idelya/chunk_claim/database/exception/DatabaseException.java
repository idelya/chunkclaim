package net.idelya.chunk_claim.database.exception;

public class DatabaseException extends Exception {
    public DatabaseException(String message) {
        super(message);
    }
}
