package net.idelya.chunk_claim.database;

import net.idelya.chunk_claim.database.exception.DatabaseException;
import net.idelya.chunk_claim.database.migration.DatabaseMigration;
import org.jetbrains.annotations.NotNull;
import org.mariadb.jdbc.MariaDbPoolDataSource;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Logger;

import static org.reflections.scanners.Scanners.SubTypes;

public class Database {
    private final Logger logger;
    private String host, database, user, password;
    private int port = 3306;
    private MariaDbPoolDataSource pool = null;

    public Database(@NotNull Logger logger) {
        this.logger = logger;
    }

    /**
     * Connect to the database with parameters in cache.
     */
    public boolean connect() {
        if (this.host == null || this.database == null || this.user == null || this.password == null) {
            logger.warning("Can't connect to database because no credentials are set.");
            return false;
        }

        return this.connect(this.host, this.port, this.database, this.user, this.password);
    }

    /**
     * Connect to the database.
     */
    public boolean connect(@NotNull String host, int port, @NotNull String database, @NotNull String user, @NotNull String password) {
        try {
            this.host = host;
            this.port = port;
            this.database = database;
            this.user = user;
            this.password = password;

            this.pool = new MariaDbPoolDataSource(String.format("jdbc:mariadb://%s:%d/%s?user=%s&password=%s&maxPoolSize=8", host, port, database, user, password));

            if (this.pool.getConnection().isValid(5)) {
                logger.info("Connected to database.");
                return true;
            }
        } catch (SQLException e) {
            logger.warning("Can't connect to database: " + e.getMessage());
        }

        return false;
    }

    /**
     * Disconnect from the database.
     */
    public void disconnect() {
        if (pool != null) {
            pool.close();
            logger.info("Disconnect from database.");
        } else {
            logger.warning("Can't disconnect from database because the plugin is not connected.");
        }
    }

    /**
     * Get the active database pool.
     *
     * @throws IllegalStateException if the plugin is not connected to database.
     */
    public @NotNull MariaDbPoolDataSource getPool() {
        if (pool != null) {
            return pool;
        }

        throw new IllegalStateException("The plugin is not connected to database.");
    }

    /**
     * Search all migrations available in the migration package and apply all migrations that are not already migrated
     * to keep the database up-to-date.
     */
    public void applyMigrations() throws DatabaseException {
        try (Connection connection = getPool().getConnection()) {
            connection.prepareStatement("CREATE TABLE IF NOT EXISTS claimchunk_migration(id BIGINT PRIMARY KEY NOT NULL, migrated_at DATE)").execute();

            ResultSet lastMigrationRs = connection.prepareStatement("SELECT * FROM claimchunk_migration WHERE migrated_at IS NOT NULL ORDER BY id DESC LIMIT 1").executeQuery();

            long lastMigrationId = 0;

            if (lastMigrationRs.first()) {
                lastMigrationId = lastMigrationRs.getLong("id");
            }

            TreeMap<Long, DatabaseMigration> migrationsToExecute = this.getAllExecutableMigrations(lastMigrationId);

            if (migrationsToExecute.size() == 0) {
                logger.info("No migrations to execute. Database is up-to-date !");
                return;
            }

            // Apply migrations in ASC order
            for (Map.Entry<Long, DatabaseMigration> migration : migrationsToExecute.entrySet()) {
                logger.info(String.format("Starting migrating version %d, \"%s\"...", migration.getKey(), migration.getValue().getDescription()));

                this.applyMigration(connection, migration.getKey(), migration.getValue());

                logger.info(String.format("Finished to migrate version %d", migration.getKey()));
            }

        } catch (SQLException e) {
            throw new DatabaseException(e.getMessage());
        }
    }

    private void applyMigration(Connection connection, long version, DatabaseMigration migration) throws SQLException {
        // Apply migration
        migration.up(connection);

        // Check if the migration have a row in migration table (for logs and technical purpose)
        PreparedStatement migrationQueryStatement = connection.prepareStatement("SELECT * FROM claimchunk_migration WHERE id = ?");
        migrationQueryStatement.setLong(1, version);

        ResultSet migrationRs = migrationQueryStatement.executeQuery();

        PreparedStatement migrationPersistStatement;

        if (migrationRs.next()) {
            migrationPersistStatement = connection.prepareStatement("UPDATE claimchunk_migration SET migrated_at = NOW() WHERE id = ?");
        } else {
            migrationPersistStatement = connection.prepareStatement("INSERT INTO claimchunk_migration (id, migrated_at) VALUES (?, NOW())");
        }

        migrationPersistStatement.setLong(1, version);
        migrationPersistStatement.executeUpdate();
    }

    private TreeMap<Long, DatabaseMigration> getAllExecutableMigrations(long lastMigrationId) {
        Reflections reflections = new Reflections("net.idelya.chunk_claim.database.migration");
        Set<Class<?>> migrations = reflections.get(SubTypes.of(DatabaseMigration.class).asClass());

        // Use a TreeMap to have migrations sorted by key
        TreeMap<Long, DatabaseMigration> migrationsToExecute = new TreeMap<>();

        for (Class<?> rawMigration : migrations) {
            try {
                Class<? extends DatabaseMigration> migrationClass = rawMigration.asSubclass(DatabaseMigration.class);

                long migrationId = Long.parseLong(migrationClass.getSimpleName().replace("Version", ""));

                // Ignore already migrated migrations
                if (lastMigrationId >= migrationId) {
                    continue;
                }
                // By reflection, instanciate the migration
                DatabaseMigration migration = migrationClass.getDeclaredConstructor().newInstance();
                // Add it to the TreeMap
                migrationsToExecute.put(migrationId, migration);
            } catch (ClassCastException e) {
                logger.warning(String.format("A migration is not of type %s.", DatabaseMigration.class));
            } catch (NumberFormatException e) {
                logger.warning("A migration name is malformed.");
            } catch (NoSuchMethodException e) {
                logger.warning("A migration is incomplete.");
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                logger.warning("A migration is unreachable.");
            }
        }

        return migrationsToExecute;
    }
}
