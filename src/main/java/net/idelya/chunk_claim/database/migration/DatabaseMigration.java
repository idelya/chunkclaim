package net.idelya.chunk_claim.database.migration;

import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.SQLException;

public interface DatabaseMigration {
    @NotNull String getDescription();

    void up(Connection connection) throws SQLException;
}
