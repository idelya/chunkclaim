# ChunkClaim

ChunkClaim is a Paper plugin to allow players to claim chunks.

## Contributing

Merge requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

## License

[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)